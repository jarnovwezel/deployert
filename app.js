express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');
var timeout = require('connect-timeout'); //express v4

// define globals
path = require('path');
fs = require('fs');
app = express();

var index = require('./routes/index');
var git = require('./routes/git');
var projects = require('./routes/projects');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(timeout('1m'));
app.use(haltOnTimedout)

app.locals = {
  site: {
    title: 'Deployert',
    menu: [
      {
        text: 'Overview',
        href: '/'
      },
      {
        text: 'Clone',
        href: '/git/clone'
      }
    ]
  },

  config: {
    projects_folder: process.cwd() + '/projects/',
    git_user: 'jarnovw',
    layout: 'layout.pug'
  }
};

app.use('/', index);
app.use('/git', git);
app.use('/projects', projects);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function haltOnTimedout (req, res, next) {
  if (!req.timedout) next()
}

module.exports = app;