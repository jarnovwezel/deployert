var Terminal = {
    $console: $('.terminal'),
    line: {
        oldString: null,
        print: function (string) {
            var $this = Terminal.line;

            var stringToOutput = string;
            stringToOutput = stringToOutput.replace($this.oldString, '');

            Terminal.$console.append('<p>' + stringToOutput+ '</p>');

            this.oldString = string;
        }
    },

    load: function (url) {
        $.get(url, function (html) {
            var $html = $(html);
            var $response = $html.find('.result');

            if(Terminal.$console.find('.default').length > 0){
                Terminal.$console.find('.default').remove();
            }

            Terminal.line.print($response.html());
        });
    }
};

$(document).ready(function () {
    var $ajaxTerminalForm = $('form.ajax');
    var $terminal = $ajaxTerminalForm.find('.terminal');

    if($terminal.length > 0) {
        var terminalScrolled = false;

        setInterval(function (){
            if(!terminalScrolled){
                $terminal.scrollTop($terminal[0].scrollHeight);
            }
        }, 300);

        $terminal.on('mouseenter', function(){
            terminalScrolled = true;
        });

        $terminal.on('mouseleave', function () {
            terminalScrolled = false;
        });
    }

    $ajaxTerminalForm.on('submit', function (e) {
        e.preventDefault();
        $terminal.removeClass('hidden');

        var oReq = new XMLHttpRequest();

        //Send the proper header information along with the request
        oReq.open("POST", $ajaxTerminalForm.attr('action'), true);
        oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        oReq.onreadystatechange = function()
        {
            var response = this.responseText;
            
            if (this.readyState == 4)
            {
                Terminal.line.print(response);

                if(this.status == 200){
                    if($ajaxTerminalForm.hasClass('reload')) {
                        Terminal.load($ajaxTerminalForm.attr('action'));
                    }

                    if($ajaxTerminalForm.hasClass('removeOnComplete')){
                        $ajaxTerminalForm.find('.alert-success').removeClass('hidden');
                    }
                }
            }

            else if(this.readyState == 3) {
                Terminal.line.print(response);
            }
        };

        oReq.send($ajaxTerminalForm.serialize());

        if($ajaxTerminalForm.hasClass('removeOnComplete')) {
            $ajaxTerminalForm.find('.form-elements-holder').remove();
        }
    });

    if($ajaxTerminalForm.hasClass('lazy')) {
        if($ajaxTerminalForm.hasClass('reload')) {
            Terminal.load($ajaxTerminalForm.attr('action'));
        }
    }
});