var router = express.Router();
var exec = require('child_process').exec;
var child;
var emptyFileData = {
    mode: 'init',
    url: '',
    user: '',
    password: ''
};

router.get('/settings/:folder', function(req, res, next) {
    var folderName = req.params.folder;
    var fileName = folderName;
    fileName = fileName.replace('.git', '.json');
    app.locals.site.page_title = 'Settings - ' + folderName;

    var configFile = app.locals.config.projects_folder + 'configs/' + fileName;

    var view = {
        settings: emptyFileData,
        projectName: folderName,
        saved: false,
        select: {
            mode: {
                init: false,
                push: false
            }
        }
    };

    if(req.query.hasOwnProperty('saved')) {
        view.saved = true;
    }

    // controleer of de config bestaat -> lees hem uit voor het aanpassen
    if (fs.existsSync(configFile)) {
        var fileData = fs.readFileSync(configFile);
        fileData = JSON.parse(fileData);
        view.settings = fileData;

        if(fileData.hasOwnProperty('mode')){
            if(fileData.mode == 'init'){
                view.select.mode.init = true;
            } else {
                view.select.mode.push = true;
            }
        }
    }

    res.render('project_detail', view);
});

router.post('/settings/:folder', function(req, res, next) {
    var folderName = req.params.folder;
    var fileName = folderName;
    fileName = fileName.replace('.git', '.json');

    var repoLocation = app.locals.config.projects_folder + 'repos/' + folderName;
    var configFile = app.locals.config.projects_folder + 'configs/' + fileName;

    for (var name in req.body) {
        if(emptyFileData.hasOwnProperty(name)) {
            emptyFileData[name] = req.body[name];
        }
    }

    // repo opzoeken
    if (fs.existsSync(repoLocation)) {

        fs.writeFile(configFile, JSON.stringify(emptyFileData), function(err) {
            if (err) {
                throw err;
            }
            console.log("The file was saved!");
        });

        var done = 0;
        var total = 0;
        var error = false;
        var commands = [];

        for (var key in emptyFileData) {
            if(key == 'mode') continue;
            if (!emptyFileData.hasOwnProperty(key)) continue;

            total++;

            commands.push('cd '+repoLocation+' && git config git-ftp.' + key + ' ' + emptyFileData[key]);
        }

        // run de commands!
        var commandsInterval = setInterval(function () {
            var command = commands[done];
            child = exec(command, function (err, stdout, stderr) {
                if (err instanceof Error) {
                    console.error(err);
                    error = true;
                }
            });

            child.on('exit', function(){ done++; });
        }, 500);

        // check of ze klaar zijn
        var checkInterval = setInterval(function () {
            if(done == total || error){
                clearInterval(checkInterval);
                clearInterval(commandsInterval);
                res.redirect('?saved=true');
            }
        }, 500);
    }
});

module.exports = router;