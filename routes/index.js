var router = express.Router();
var path = require('path');

/* GET home page. */
router.get('/', function(req, res, next) {
  app.locals.site.page_title = 'Overview';
  app.locals.repos = getDirectories(app.locals.config.projects_folder+'repos/');

  res.render('index');
});

function getDirectories (srcpath) {
  return fs.readdirSync(srcpath)
          .filter(file => fs.statSync(path.join(srcpath, file)).isDirectory())
}

module.exports = router;