var router = express.Router();
var exec = require('child_process').exec;
var child;
var view = {
  folder: '',
  git_ssh: ''
};

router.get('/clone', function(req, res, next) {
  app.locals.site.page_title = 'Clone';

  res.render('git_clone', view);
});

router.post('/clone', function(req, res, next) {
  res.header('transfer-encoding', 'chunked');
  res.set('Content-Type', 'text/html');

  var url = req.body.url;
  url = url.split('@');
  url = url[url.length-1];

  var checkoutUrl = 'https://'+ app.locals.config.git_user + '@' + url;
  if(url == null || url == '') return false;

  // folder is het laatste element in de url
  var folder = url.split("/");
  folder = folder[folder.length-1];
  if(folder == null || folder == '') return false;

  //base folder zetten
  var repoLocation = app.locals.config.projects_folder + 'repos/' + folder;

  // status update richting de view
  res.write('Starting git...');

  // controlle of de map bestaat -> aanmaken
  if (!fs.existsSync(repoLocation)) {
    fs.mkdirSync(repoLocation);
    res.write('Folder: '+ folder+' created... <br/>');
  }

  // controlle of de map bestaat
  if (fs.existsSync(repoLocation)) {
    res.write('Starting checkout...');

    // repo clonen in de aangemaakte map
    require('simple-git')()
      .clone(checkoutUrl, repoLocation, null, function(err, log){
        if (err){
          res.status(500);
          res.write('<br/>');
          res.write(err);
          res.write('<br/>');
          res.end('Clone failed...');
        }
      })
      .then(function () {
        res.end('Clone task completed.');
      });
  }
});

router.get('/deploy/:folder', function(req, res, next) {
  // bool zetten of het een xhr request is
  app.locals.isXhr = req.xhr;

  //base folder zetten
  var folder = req.params.folder;
  var repoLocation = app.locals.config.projects_folder + 'repos/' + folder;
  app.locals.site.page_title = 'Deploy - ' + folder;
  view.folder = folder;

  // controlle of de map bestaat
  if (!fs.existsSync(repoLocation)) {
    res.status(500).send('Repo doesn\'t exist!')
  }

  // geen ajax request -> normale view rederen
  if(!app.locals.isXhr) {
    res.render('git_deploy', view);
  }

  // wel een ajax request -> haal de repo status op
  else {
    var simpleGit = require('simple-git')(repoLocation);
    view.git = {};

    // update de repo status
    child = exec('cd '+ repoLocation +' && git remote -v update', function (err, stdout, stderr) {
      if (err instanceof Error){
        throw err;
      }

      // haal de commit info op
      else {
        simpleGit
          .log(function (err, log) {
            view.git.log = log;
          })
          .status(function (err, status) {
            view.git.status = status;
          })
          .then(function () {
            res.render('git_deploy', view);
          });
      }
    });
  }
});

router.post('/deploy/:folder', function(req, res, next) {
  var folderName = req.params.folder;
  var fileName = folderName;
  fileName = fileName.replace('.git', '.json');

  // repo locatie zetten
  var repoLocation = app.locals.config.projects_folder + 'repos/' + folderName;
  var configFile = app.locals.config.projects_folder + 'configs/' + fileName;

  var config = {};
  var mode = 'init';
  var possibleModes = ['init', 'push'];

  if (fs.existsSync(configFile)) {
    config = fs.readFileSync(configFile);
    config = JSON.parse(config);
  }

  if(config.hasOwnProperty('mode') && possibleModes.indexOf(config.mode) > 0){
    mode = config.mode;
  }

  res.header('transfer-encoding', 'chunked');
  res.set('Content-Type', 'text/html');
  res.write('Starting pull...');

  require('simple-git')(repoLocation)

      // repo eerst updaten!
      .pull(function (err, pull) {
        if (err instanceof Error){
          res.write(err);
          res.write('<br/>');
          res.end('Deployment failed...');
        }

        // geen error, dus gewoon door gaan met het script.
        else if(pull.hasOwnProperty('summary')){
          console.log(pull);

          var data = pull.summary;

          var result = [];
          for (var key in data) {
            result.push(key + ': ' + data[key]);
          }

          // data tonen
          res.write(result.join('<br/>'));
        }
      })

      // dan pushen naar de productie
      .then(function () {
        var command = 'cd '+ repoLocation +' && git ftp ' + mode;
        res.write('Starting deployment...');

        console.log(command);

        child = exec(command, function (err, out, code) {
          if (err instanceof Error){
            console.error(err);
          }
        });

        child.stdout.on('data', function (data) {
          res.write(data.toString());
          res.write('<br/>');
        });

        child.stderr.on('data', function (data) {
          res.write(data.toString());
          res.write('<br/>');
        });

        child.on('exit', function (code) {
          console.log('child process exited with code ' + code.toString());
          if(code.toString() == 0){
            res.end('Upload completed.');
          }
        });
    });
});

module.exports = router;